#include <iostream>

using namespace std;

int main(){
	/* Ejercicio 2 - Escriba un programa que diga si un numero
       entero es positivo, negativo o neutro*/
	int Numero;
	
	cout<<"Ingrese un numero"<<endl;
	cin>>Numero;
	
	if(Numero >0){
		cout<<"Su numero es positivo"<<endl;
	}else if(Numero == 0){
		cout<<"Su numero es igual a 0"<<endl;
	}else{
		cout<<"Su numero es negativo"<<endl;
	}
	
	return 0;
}
