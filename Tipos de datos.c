#include <stdio.h>

void main() {
    /* Tipos de datos para funciones, variables y constantes */

    void MiFuncion();// Tipo de dato para funciones que no debuelven ningun valor

    char Letra;      // 8 bits - De 0 a 255
    int NumEntero;   // 16 bits - De -32.768 a 32.767
    float NumReal;   // 32 bits - 3.4E -38 a 3.4E +38
    double NumReal2; // 64 bits - 1.7E -308 a 1.7E +308

    /* Modificadores para tipos de datos en funciones, variables y contantes */

    unsigned char Letra2; // 8 bits - De 0 a 255
    signed char Letra3;   // 8 bits - De -128 a 127

    unsigned int NumEntero2; //  16 bits - De 0 a 65.535
    signed int NumEntero3;   //  16 bits - De -32.768 a 32.767
    short int NumEntero4;    //  16 bits - De -32.768 a 32.767

    unsigned short int NumEntero5; //  16 bits - De 0 a 65.535
    signed short int NumEntero6;   //  16 bits -32.768 a 32.767

    long int NumEntero7;    //  16 bits -2147483648 a 2147483647
    long double NumReal3;   //  64 bits - De 1,7E -308 a 1,7E +308

}
